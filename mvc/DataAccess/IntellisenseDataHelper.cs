﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.DTO;

namespace DataAccess
{
    public class IntellisenseDataHelper
    {
        private ryokoEntities _entities = new ryokoEntities();
        private ryokoTestEntities _entitiesTest = new ryokoTestEntities();
        private bool isTestServer = false&&Environment.MachineName.ToLower().Contains("davidsocha");

        public List<CarDetailDTO> GetAllCarDetails()
        {
            if (isTestServer)
            {
                List<CarDetailDTO> details = _entitiesTest.CarDetails.ToList().Select(d => CarDetailDTO.ConvertToDTO(d)).ToList();
                return details;
            }
            else
            {
                List<CarDetailDTO> details = _entities.CarDetails.ToList().Select(d => CarDetailDTO.ConvertToDTO(d)).ToList();
                return details;
            }
        }

        public List<ClientDTO> GetAllClients()
        {
            if (isTestServer)
            {
                List<ClientDTO> details = _entitiesTest.Clients.ToList().Select(d => ClientDTO.ConvertToDTO(d)).ToList();
                return details;
            }
            else
            {
                List<ClientDTO> details = _entities.Clients.ToList().Select(d => ClientDTO.ConvertToDTO(d)).ToList();
                return details;
            }
        }

        public List<CompanyDTO> GetAllRentalCompanies()
        {
            if (isTestServer)
            {
                List<CompanyDTO> companies = _entitiesTest.CarExpenses.ToList().Select(d => new CompanyDTO() { Name = d.RentalCompany }).ToList();
                return companies;
            }
            else
            {
                List<CompanyDTO> companies = _entities.CarExpenses.ToList().Select(d => new CompanyDTO() { Name = d.RentalCompany }).ToList();
                return companies;
            }
        }

        public List<CompanyDTO> GetAllFlightCompanies()
        {
            if (isTestServer)
            {
                List<CompanyDTO> companies = _entitiesTest.PlaneExpenses.ToList().Select(d => new CompanyDTO() { Name = d.FlightCompany }).ToList();
                return companies;
            }
            else
            {
                List<CompanyDTO> companies = _entities.PlaneExpenses.ToList().Select(d => new CompanyDTO() { Name = d.FlightCompany }).ToList();
                return companies;
            }
        }

        public List<CompanyDTO> GetAllTrainCompanies()
        {
            if (isTestServer)
            {
                List<CompanyDTO> companies = _entitiesTest.TrainExpenses.ToList().Select(d => new CompanyDTO() { Name = d.TrainCompany }).ToList();
                return companies;
            }
            else
            {
                List<CompanyDTO> companies = _entities.TrainExpenses.ToList().Select(d => new CompanyDTO() { Name = d.TrainCompany }).ToList();
                return companies;
            }
        }
    }
}
