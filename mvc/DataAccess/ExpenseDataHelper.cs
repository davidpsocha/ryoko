﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.DTO;

namespace DataAccess
{
    public class ExpenseDataHelper
    {
        private ryokoEntities _entities = new ryokoEntities();
        private ryokoTestEntities _entitiesTest = new ryokoTestEntities();
        private bool isTestServer = false&&Environment.MachineName.ToLower().Contains("davidsocha"); 

        private Client CheckForExisting(Client client)
        {
            if (client == null)
                return null;

            if(isTestServer)
            {
                var existing = _entitiesTest.Clients.FirstOrDefault(c => c.Name.ToLower().Equals(client.Name.ToLower()));
                return existing ?? client;
            }
            else
            {
                var existing = _entities.Clients.FirstOrDefault(c => c.Name.ToLower().Equals(client.Name.ToLower()));
                return existing ?? client;
            }
        }

        private CarDetail CheckForExisting(CarDetail detail)
        {
            if (detail == null)
                return null;

            if (isTestServer)
            {
                var existing = _entitiesTest.CarDetails
                    .FirstOrDefault(c => c.Make.ToLower().Equals(detail.Make.ToLower()) && c.Model.ToLower().Equals(detail.Model.ToLower()));
                return existing ?? detail;
            }
            else
            {
                var existing = _entities.CarDetails
                    .FirstOrDefault(c => c.Make.ToLower().Equals(detail.Make.ToLower()) && c.Model.ToLower().Equals(detail.Model.ToLower()));
                return existing ?? detail;
            }
        }

        public List<List<ExpenseDTO>> GetUserExpenses(string userid)
        {
            List<List<ExpenseDTO>> result = new List<List<ExpenseDTO>>();

            if (isTestServer)
            {
                
                List<ExpenseDTO> carExp = _entitiesTest.CarExpenses.Where(u => u.UserId == userid).ToList().Select(x => ExpenseDTO.ConvertFromDAO(x)).OrderBy(y => y.Date).ToList();
                List<ExpenseDTO> planeExp = _entitiesTest.PlaneExpenses.Where(u => u.UserId == userid).ToList().Select(x => ExpenseDTO.ConvertFromDAO(x)).OrderBy(y => y.Date).ToList();
                List<ExpenseDTO> trainExp = _entitiesTest.TrainExpenses.Where(u => u.UserId == userid).ToList().Select(x => ExpenseDTO.ConvertFromDAO(x)).OrderBy(y => y.Date).ToList();
                
                result.Add(carExp);
                result.Add(planeExp);
                result.Add(trainExp);
            }
            else
            {
                List<ExpenseDTO> carExp = _entities.CarExpenses.Where(u => u.UserId == userid).ToList().Select(x => ExpenseDTO.ConvertFromDAO(x)).OrderBy(y => y.Date).ToList();
                List<ExpenseDTO> planeExp = _entities.PlaneExpenses.Where(u => u.UserId == userid).ToList().Select(x => ExpenseDTO.ConvertFromDAO(x)).OrderBy(y => y.Date).ToList();
                List<ExpenseDTO> trainExp = _entities.TrainExpenses.Where(u => u.UserId == userid).ToList().Select(x => ExpenseDTO.ConvertFromDAO(x)).OrderBy(y => y.Date).ToList();
                
                result.Add(carExp);
                result.Add(planeExp);
                result.Add(trainExp);
            }
            
            return result;
        }

        public void SaveCarExpense(CarDTO dto)
        {
            if (isTestServer)
            {
                CarExpense exp = CarDTO.ConvertFromDTO(dto);
                exp.Client = CheckForExisting(exp.Client);
                exp.CarDetail = CheckForExisting(exp.CarDetail);
                _entitiesTest.CarExpenses.Add(exp);
                _entitiesTest.SaveChanges();

            }
            else
            {
                CarExpense exp = CarDTO.ConvertFromDTO(dto);
                exp.Client = CheckForExisting(exp.Client);
                exp.CarDetail = CheckForExisting(exp.CarDetail);
                _entities.CarExpenses.Add(exp);
                _entities.SaveChanges();
            }
        }

        public void SavePlaneExpense(PlaneDTO dto)
        {
            if (isTestServer)
            {
                PlaneExpense exp = PlaneDTO.ConvertFromDTO(dto);
                exp.Client = CheckForExisting(exp.Client);
                _entitiesTest.PlaneExpenses.Add(exp);
                _entitiesTest.SaveChanges();

            }
            else
            {
                PlaneExpense exp = PlaneDTO.ConvertFromDTO(dto);
                exp.Client = CheckForExisting(exp.Client);
                _entities.PlaneExpenses.Add(exp);
                _entities.SaveChanges();
            }
        }

        public void SaveTrainExpense(TrainDTO dto)
        {
            if (isTestServer)
            {
                TrainExpense exp = TrainDTO.ConvertFromDTO(dto);
                exp.Client = CheckForExisting(exp.Client);
                _entitiesTest.TrainExpenses.Add(exp);
                _entitiesTest.SaveChanges();
            }
            else
            {
                TrainExpense exp = TrainDTO.ConvertFromDTO(dto);
                exp.Client = CheckForExisting(exp.Client);
                _entities.TrainExpenses.Add(exp);
                _entities.SaveChanges();
            }
        }
    }
}
