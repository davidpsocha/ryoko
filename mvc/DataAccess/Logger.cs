﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class Logger
    {
        private static bool isTestServer = false&&Environment.MachineName.ToLower().Contains("davidsocha");

        public static void LogMessage(string message)
        {
            try
            {
                if (isTestServer)
                {
                    string toLog = message.Length <= 2048 ? message : message.Substring(0, 2048);
                    ryokoTestEntities _entitiesTest = new ryokoTestEntities();
                    _entitiesTest.ErrorLogTests.Add(new ErrorLogTest() { Message = toLog });
                    _entitiesTest.SaveChanges();
                }
                else
                {
                    string toLog = message.Length <= 256 ? message : message.Substring(0, 256);
                    ryokoEntities _entities = new ryokoEntities();
                    _entities.ErrorLogs.Add(new ErrorLog() { Message = toLog });
                    _entities.SaveChanges();
                }
            }
            catch(Exception)
            {

            }
        }
    }
}
