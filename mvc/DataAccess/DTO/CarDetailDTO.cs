﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace DataAccess.DTO
{
    [DataContract]
    public class CarDetailDTO : IValidatableObject
    {
        [DataMember]
        public string Make { get; set; }

        [DataMember]
        public string Model { get; set; }

        public static CarDetail ConvertFromDTO(CarDetailDTO dto)
        {
            var result = new CarDetail()
            {
                Make = dto.Make,
                Model = dto.Model
            };

            return result;
        }

        public static CarDetailDTO ConvertToDTO(CarDetail dao)
        {
            var result = new CarDetailDTO()
            {
                Make = dao.Make,
                Model = dao.Model
            };

            return result;
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if(string.IsNullOrWhiteSpace(Make))
            {
                errors.Add(new ValidationResult("Make is required."));
            }

            if (string.IsNullOrWhiteSpace(Model))
            {
                errors.Add(new ValidationResult("Model is required."));
            }

            return errors;
        }
    }
}