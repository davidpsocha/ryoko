﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace DataAccess.DTO
{
    [DataContract]
    public class ExpenseDTO
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public string Type { get; set; }

        [DataMember]
        public double Cost { get; set; }

        public static ExpenseDTO ConvertFromDAO(CarExpense exp)
        {
            var result = new ExpenseDTO()
            {
                Id = exp.Id,
                Date = exp.Date,
                Type = "car",
                Cost = exp.Cost
            };
            return result;
        }

        public static ExpenseDTO ConvertFromDAO(PlaneExpense exp)
        {
            var result = new ExpenseDTO()
            {
                Id = exp.Id,
                Date = exp.Date,
                Type = "plane",
                Cost = exp.Cost
            };
            return result;
        }

        public static ExpenseDTO ConvertFromDAO(TrainExpense exp)
        {
            var result = new ExpenseDTO()
            {
                Id = exp.Id,
                Date = exp.Date,
                Type = "train",
                Cost = exp.Cost
            };
            return result;
        }
    }
}