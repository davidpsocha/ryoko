﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace DataAccess.DTO
{
    [DataContract]
    public class ClientDTO : IValidatableObject
    {
        [DataMember]
        public string Name { get; set; }

        public static Client ConvertFromDTO(ClientDTO dto)
        {
            var result = new Client()
            {
                Name = dto.Name
            };

            return result;
        }

        public static ClientDTO ConvertToDTO(Client dao)
        {
            var result = new ClientDTO()
            {
                Name = dao.Name
            };

            return result;
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if(string.IsNullOrWhiteSpace(Name))
            {
                errors.Add(new ValidationResult("Name is required."));
            }

            return errors;
        }
    }
}