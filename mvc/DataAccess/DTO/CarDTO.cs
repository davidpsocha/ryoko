﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace DataAccess.DTO
{
    [DataContract]
    public class CarDTO : IValidatableObject
    {
        [DataMember]
        public string UserId { get; set; }
        
        [DataMember]
        public string Client { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public string RentalCompany { get; set; }

        [DataMember]
        public string Make { get; set; }

        [DataMember]
        public string Model { get; set; }

        [DataMember]
        public int Mileage { get; set; }

        [DataMember]
        public double Cost { get; set; }

        public static CarExpense ConvertFromDTO(CarDTO dto)
        {
            DateTime convertedTime = TimeZoneInfo.ConvertTimeToUtc(DateTime.SpecifyKind(dto.Date, DateTimeKind.Local), TimeZoneInfo.Local);
            var result = new CarExpense()
            {
                Client = new Client() { Name = dto.Client },
                Cost = dto.Cost,
                Date = convertedTime,
                CarDetail = new CarDetail() { Make = dto.Make, Model = dto.Model },
                Mileage = dto.Mileage,
                RentalCompany = dto.RentalCompany,
                UserId = dto.UserId
            };

            return result;
        }

        public static CarDTO ConvertToDTO(CarExpense dao)
        {
            DateTime convertedTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.SpecifyKind(dao.Date, DateTimeKind.Utc), TimeZoneInfo.Local);
            var result = new CarDTO()
            {
                Client = dao.Client != null ? dao.Client.Name : "",
                Cost = dao.Cost,
                Date = convertedTime,
                Make = dao.CarDetail != null ? dao.CarDetail.Make : "",
                Model = dao.CarDetail != null ? dao.CarDetail.Model : "",
                Mileage = dao.Mileage,
                RentalCompany = dao.RentalCompany,
                UserId = dao.UserId
            };

            return result;
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if(string.IsNullOrWhiteSpace(Client))
            {
                errors.Add(new ValidationResult("Client name is required."));
            }

            if (Date == DateTime.MinValue || Date == DateTime.MaxValue)
            {
                errors.Add(new ValidationResult("Date is required."));
            }

            if (string.IsNullOrWhiteSpace(RentalCompany))
            {
                errors.Add(new ValidationResult("Rental Company is required."));
            }

            if (string.IsNullOrWhiteSpace(Make))
            {
                errors.Add(new ValidationResult("Make name is required."));
            }

            if (Mileage <= 0)
            {
                errors.Add(new ValidationResult("Mileage is required."));
            }

            if (Cost <= 0)
            {
                errors.Add(new ValidationResult("Cost is required."));
            }

            return errors;
        }
    }
}